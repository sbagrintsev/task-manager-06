package ru.tsc.bagrintsev.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.tsc.bagrintsev.tm.constant.CommandLineConst.*;
import static ru.tsc.bagrintsev.tm.constant.RunTimeConst.*;

/**
 * @author Sergey Bagrintsev
 * @version 1.6.0
 */

public final class Application {

    public static void main(final String[] args) throws IOException {
        processOnStart(args);

        showWelcome();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                final String command = reader.readLine();
                processOnTheGo(command);
            }
        }
    }

    private static void processOnStart(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case HELP_SHORT:
                showOnStartHelp();
                break;
            case VERSION_SHORT:
                showVersion();
                break;
            case ABOUT_SHORT:
                showAbout();
                break;
            default:
                showOnStartError(arg);
                showOnStartHelp();
        }
    }

    private static void processOnTheGo(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case HELP:
                showHelp();
                break;
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case EXIT:
                close();
                break;
            default:
                showError(command);
                showHelp();
        }
    }

    private static void processOnStart(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        processOnStart(arg);
        close();
    }

    private static void showWelcome() {
        System.out.println("*** Welcome to Task Manager ***");
    }

    private static void showHelp() {
        System.out.println("[Command List]");
        System.out.printf("%-25s Print version.\n", VERSION);
        System.out.printf("%-25s Print help.\n", HELP);
        System.out.printf("%-25s Print about.\n", ABOUT);
        System.out.printf("%-25s Close application.\n", EXIT);
    }

    private static void showOnStartHelp() {
        System.out.println("[StartUp Arguments]");
        System.out.printf("%-25s Print version.\n", VERSION_SHORT);
        System.out.printf("%-25s Print help.\n", HELP_SHORT);
        System.out.printf("%-25s Print about.\n", ABOUT_SHORT);
    }

    private static void showVersion() {
        System.out.println("task-manager version 1.6.0");
    }

    private static void showAbout() {
        System.out.println("[Author]");
        System.out.println("Name: Sergey Bagrintsev");
        System.out.println("E-mail: sbagrintsev@t1-consulting.com");
    }

    private static void close() {
        System.exit(0);
    }

    private static void showError(final String arg) {
        System.err.printf("Error! Command '%s' is not supported...\n", arg);
    }

    private static void showOnStartError(final String arg) {
        System.err.printf("Error! Argument '%s' is not supported...\n", arg);
    }

}
