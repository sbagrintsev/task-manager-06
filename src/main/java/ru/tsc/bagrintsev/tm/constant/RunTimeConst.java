package ru.tsc.bagrintsev.tm.constant;

public class RunTimeConst {

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String HELP = "help";

    public static final String EXIT = "exit";
}
