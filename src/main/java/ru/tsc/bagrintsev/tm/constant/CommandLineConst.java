package ru.tsc.bagrintsev.tm.constant;

public final class CommandLineConst {

    public static final String VERSION_SHORT = "-v";

    public static final String ABOUT_SHORT = "-a";

    public static final String HELP_SHORT = "-h";

}
